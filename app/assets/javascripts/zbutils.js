function expandContract(elementId) {
    $('#' + elementId).toggle();
    signElement = $('#sign_' + elementId);
    if (signElement.html().indexOf('plus') > -1) {
      signElement.html('<span class="glyphicon glyphicon-minus" aria-hidden="true" />');
    } else {
      signElement.html('<span class="glyphicon glyphicon-plus" aria-hidden="true" />');
    }
}