module ZBUtils
  
  module ViewHelpers

    def zb_show_field(label, value)
      """<div class=\"field\">
          <label>#{label}</label>
          #{value.nil? ? '-' : value}&nbsp;
        </div>""".html_safe
    end

    def zb_expandable_td(id)
      """<td class=\"expand\">
            <a href=\"javascript:expandContract('id_#{id}')\">
              <span id=\"sign_id_#{id}\">
                <span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\" />
              </span>
            </a>
          </td>""".html_safe
    end

    def zb_weekday_name(date=Date.today)
      Date::DAYNAMES[date.wday]
    end

    # Converte uma string tipo "IstoEhUmTeste" em "Isto Eh Um Teste"
    def zb_spaced_camelcase(str)
      str.underscore.split('_').collect{|c| c.capitalize}.join(' ')
    end

  end
end