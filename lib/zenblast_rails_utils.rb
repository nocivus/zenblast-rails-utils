require "zenblast_rails_utils/version"
require "zenblast_rails_utils/view_helpers"

module ZBUtils
  module Rails
    class Engine < ::Rails::Engine
      initializer "zenblast_rails_utils.view_helpers" do
        ActionView::Base.send :include, ViewHelpers
      end
    end
  end
end